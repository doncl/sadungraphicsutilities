//
//  DrawingUtil.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.
import UIKit
import QuartzCore

func drawString(string: String, centeredInRect rect : CGRect,
  withFont font: UIFont, andColor color: UIColor) {
  
  guard let context = UIGraphicsGetCurrentContext() else {
    return
  }
  
  // Calculate string size
  let stringSize = string.size(withAttributes: [NSAttributedString.Key.font : font])
  
  // Find the target rectangle
  let target = CGRect.rectAroundCenter(center: rect.center, size: stringSize)
  
  // Draw the string
  context.saveGState()
  color.set()
  string.draw(in: target, withAttributes:[NSAttributedString.Key.font : font])
  context.restoreGState()
}

