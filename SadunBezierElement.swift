//
//  BezierElement.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/2/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore

typealias PathBlock = (CGPoint) -> (CGPoint)

public class BezierElement : NSObject {
  var type : CGPathElementType = .moveToPoint   // default
  var point : CGPoint = CGRect.null.origin
  var controlPoint1 : CGPoint = CGPoint.Nil
  var controlPoint2 : CGPoint = CGPoint.Nil
  
  override init () {
    super.init()
  }
  
  class func element(with pathElement : CGPathElement) -> BezierElement {
    let newElement = BezierElement()
    newElement.type = pathElement.type
    
    switch newElement.type {
    case .closeSubpath:
      break
    case .moveToPoint:
      fallthrough
    case .addLineToPoint:
      newElement.point = pathElement.points[0]
    case .addQuadCurveToPoint:
      newElement.point = pathElement.points[1]
      newElement.controlPoint1 = pathElement.points[0]
      break
    case .addCurveToPoint:
      newElement.point = pathElement.points[2]
      newElement.controlPoint1 = pathElement.points[0]
      newElement.controlPoint2 = pathElement.points[1]
      break
    @unknown default:
      print("Unknown case \(newElement.type)")
    }
    
    return newElement
  }
}

//MARK: NSCopying
extension BezierElement : NSCopying {
  public func copy(with zone: NSZone? = nil) -> Any {
    
    let theCopy = BezierElement()
    theCopy.point = point
    theCopy.controlPoint1 = controlPoint1
    theCopy.controlPoint2 = controlPoint2
    
    return theCopy
  }
}

//MARK: Path
extension BezierElement {
  func element(byApplying pathBlock : PathBlock?) -> BezierElement {
    let output = copy() as! BezierElement
    
    guard let pathBlock = pathBlock else {
      return output
    }
    
    if false == output.point.isNil {
      output.point = pathBlock(output.point)
    }
    if false == output.controlPoint1.isNil {
      output.controlPoint1 = pathBlock(output.controlPoint1)
    }
    if false == output.controlPoint2.isNil {
      output.controlPoint2 = pathBlock(output.controlPoint2)
    }
    return output
  }
  
  func add(to path: UIBezierPath) {
    switch type {
    case .closeSubpath:
      path.close()
    case .moveToPoint:
      path.move(to: point)
    case .addLineToPoint:
      path.addLine(to: point)
    case .addQuadCurveToPoint:
      path.addQuadCurve(to: point, controlPoint: controlPoint1)
    case .addCurveToPoint:
      path.addCurve(to: point, controlPoint1: controlPoint1,
        controlPoint2: controlPoint2)
    @unknown default:
      print("Unknown case \(type)")
    }
  }
}

//MARK: Strings
extension BezierElement {
  var stringValue : String {
    switch type {
    case .closeSubpath:
      return "Close Path"
    case .moveToPoint:
      return "Move to point \(NSCoder.string(for: point))"
    case .addLineToPoint:
      return "Add line to point \(NSCoder.string(for: point))"
    case .addQuadCurveToPoint:
      return "Add quad curve to point \(NSCoder.string(for: point)) with " +
        "control point \(NSCoder.string(for: controlPoint1))"
    case .addCurveToPoint:
      return "Add Curve to point \(NSCoder.string(for: point)) with control " +
        "points \(NSCoder.string(for: controlPoint1)) and " +
        "\(NSCoder.string(for: controlPoint2))"
    @unknown default:
      print("Unknown case \(type)")
      return ""
    }
  }
  
  // This is debug code.
  func showTheCode() {
    switch type {
    case .closeSubpath:
      print("  path.close()")
    case .moveToPoint:
      print(" path.move(to: \(point.x), \(point.y)")
    case .addLineToPoint:
      print(" path.addLine(to: \(point.x), \(point.y)")
      break
    case .addQuadCurveToPoint:
      print(" path.addQuadCurve(to: \(point.x), \(point.y), controlPoint: " +
        "\(controlPoint1.x), \(controlPoint1.y)")
      break
    case .addCurveToPoint:
      print(" path.addCurve(to: \(point.x), \(point.y), controlPoint1: " +
        "\(controlPoint1.x), \(controlPoint1.y), controlPoint2: " +
        "\(controlPoint2.x), \(controlPoint2.y)")
      break
    @unknown default:
      print("Unknown case \(type)")
    }
  }
}

//MARK:  Distances
extension BezierElement {
  
  func distance(from targetPoint: CGPoint, withStartPoint startPoint : CGPoint)
    -> CGFloat {
    
    switch type {
    case .moveToPoint:
      return 0.0
      
    case .closeSubpath:
      return targetPoint.distance(from: startPoint)
      
    case .addLineToPoint:
      return targetPoint.distance(from: self.point)
      
    case .addCurveToPoint:
      return targetPoint.cubicBezierLength(controlPoint1: self.controlPoint1,
        controlPoint2: self.controlPoint2, endPoint: self.point)
      
    case .addQuadCurveToPoint:
      return targetPoint.quadBezierLength(controlPoint1: self.controlPoint1,
        endPoint: self.point)
    @unknown default:
      print("Unknown case \(type)")
      return 0
    }
  }
}
























