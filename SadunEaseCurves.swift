//
//  SadunEaseCurves.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import Foundation
import UIKit

class SadunEaseCurves {
  class func EaseIn(currentTime : CGFloat, factor : Int) -> CGFloat {
    return pow(currentTime, CGFloat(factor))
  }
  
  class func EaseOut(currentTime : CGFloat, factor : Int) -> CGFloat {
    return CGFloat(1.0) - pow(CGFloat(1.0) - currentTime, CGFloat(factor))
  }
  
  class func EaseInOut(currentTime ct: CGFloat, factor: Int) -> CGFloat {
    var currentTime = ct * 2.0
    if currentTime < CGFloat(1.0) {
      return CGFloat(0.5) * pow(currentTime, CGFloat(factor))
    }
    currentTime -= CGFloat(2.0)
    if factor % 2 > 0 {
      return CGFloat(0.5) * (pow(currentTime, CGFloat(factor)) + CGFloat(2.0))
    }
    return CGFloat(0.5) * (CGFloat(2.0) - pow(currentTime, CGFloat(factor)))
  }
}
