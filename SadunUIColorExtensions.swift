//
//  UIColorExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//

import UIKit

public extension UIColor {
  static func interpolate(between c1: UIColor, and c2: UIColor, by amt: CGFloat) -> UIColor {
    var r1 : CGFloat = 0.0
    var g1 : CGFloat = 0.0
    var b1 : CGFloat = 0.0
    var a1 : CGFloat = 0.0
    var r2 : CGFloat = 0.0
    var g2 : CGFloat = 0.0
    var b2 : CGFloat = 0.0
    var a2 : CGFloat = 0.0
    
    if c1.cgColor.numberOfComponents == 4 {
      c1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
    } else {
      c1.getWhite(&r1, alpha: &a1)
      g1 = r1
      b1 = r1
    }
    
    if c2.cgColor.numberOfComponents == 4 {
      c2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
    } else {
      c2.getWhite(&r2, alpha: &a2)
      g2 = r2
      b2 = r2
    }
    
    let r : CGFloat = (r2 * amt) + (r1 * (1.0 - amt))
    let g : CGFloat = (g2 * amt) + (g1 * (1.0 - amt))
    let b : CGFloat = (b2 * amt) + (b1 * (1.0 - amt))
    let a : CGFloat = (a2 * amt) + (a1 * (1.0 - amt))
    
    let interpolatedResult : UIColor = UIColor(red: r, green: g, blue: b, alpha: a)
    return interpolatedResult
  }
  
  func scaleBrightness(amount: CGFloat) -> UIColor {
    var h : CGFloat = 0
    var s : CGFloat = 0
    var v : CGFloat = 0
    var a : CGFloat = 0
    
    getHue(&h, saturation: &s, brightness: &v, alpha: &a)
    
    let v1 = (v * amount).clamp(min: 0, max: 1)
    return UIColor(hue: h, saturation: s, brightness: v1, alpha: a)
    
  }
}

extension UIColor {
  fileprivate static var noiseImage : UIImage? = nil
  
  // N.B. NOT THREADSAFE.
  static var noise : UIColor {
    assert(Thread.isMainThread, "DO NOT CALL OFF MAIN THREAD")
    
    if let noiseImage = noiseImage {
      return UIColor(patternImage: noiseImage)
    }
  
    let height : Int = 128
    let width : Int = 128
    let size = CGSize(width: CGFloat(width), height: CGFloat(height))
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    for j in 0..<height {
      for i in 0..<width {
        let r = CGRect(x: i, y: j, width: 1, height: 1)
        let path = UIBezierPath(rect: r)
        let ran = arc4random()
        let num = CGFloat(ran)
        let den = CGFloat(MAXFLOAT)
        let level = num / den
        let randomColor = UIColor(white: level, alpha: 1.0)
        path.fill(withColor: randomColor)
      }
    }
    guard let noiseimg = UIGraphicsGetImageFromCurrentImageContext() else {
      fatalError("IMPOSSIBLE. This should never happen.")
    }
    noiseImage = noiseimg
    return UIColor(patternImage: noiseimg)
  }
  
  var contrast : UIColor {
    if let colorSpace = cgColor.colorSpace {
      let numComponents = colorSpace.numberOfComponents
      if 3 == numComponents {
        var r : CGFloat = 0
        var g : CGFloat = 0
        var b : CGFloat = 0
        var a : CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let luminance : CGFloat = r * 0.2126 + g * 0.7152 + b * 0.0722
        return luminance > 0.5 ? .black : .white
      }
    }
    
    
    var w : CGFloat = 0
    var a : CGFloat = 0
    getWhite(&w, alpha: &a)
    return w > 0.5 ? .black : .white
  }
}



