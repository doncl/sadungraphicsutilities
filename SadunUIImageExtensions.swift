//
//  UIImageExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.
//

import UIKit
import QuartzCore

public extension UIImage {
  func grayscaleVersion() -> UIImage? {
    guard let cgImage = cgImage else {
      return nil
    }
    // Establish grayscale colorspace
    let colorSpace = CGColorSpaceCreateDeviceGray()
    
    // Extents are integers
    let width = Int(size.width)
    let height = Int(size.height)
    
    // Build context; one byte per pixel, no alpha
    guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: 8,
      bytesPerRow: width, space: colorSpace, bitmapInfo: CGImageAlphaInfo.none.rawValue) else {
        
      return nil
    }
    
    // No release of this context necessary - Core Foundation objects are automagically 
    // ARC - memory managed now.
    
    // Replicate image using new color space
    let rect = CGRect.from(size: size)
    context.draw(cgImage, in: rect)
    
    guard let imageRef = context.makeImage() else {
      return nil
    }
    
    // Return the grayscale image
    let output = UIImage(cgImage: imageRef)
    return output
  }
  
  func watermarked(withText watermark : String, targetSize : CGSize, font: UIFont) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    
    defer {
      UIGraphicsEndImageContext()
    }
    
    guard let context = UIGraphicsGetCurrentContext() else  {
      return nil
    }
    
    // Draw the original image into the context
    let targetRect = CGRect.from(size: targetSize)
    let srcRect = CGRect.from(size: self.size)
    let imgRect = srcRect.fill(into: targetRect)
    draw(in: imgRect)
    
    let size = watermark.size(withAttributes: [NSAttributedString.Key.font : font])
    let stringRect = CGRect.from(size: size).centeredInRect(mainRect: targetRect)
    
    // Rotate the context
    let center = targetRect.center
    context.translateBy(x: center.x, y: center.y)
    context.rotate(by: CGFloat.pi / 4.0)
    context.translateBy(x: -center.x, y: -center.y)
    
    // Draw the string, using a blend mode
    context.setBlendMode(.difference)
    watermark.draw(in: stringRect, withAttributes: [
      NSAttributedString.Key.font : font,
      NSAttributedString.Key.foregroundColor : UIColor.white,
    ])
    
    guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return image
  }
  
  func extract(rect: CGRect) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 1)
    defer {
      UIGraphicsEndImageContext()
    }
    
    let destRect = CGRect(x: -rect.origin.x, y: -rect.origin.y, width: size.width,
      height: size.height)
    
    draw(in: destRect)
    
    guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    return newImage
  }
  
  func thumb(targetSize : CGSize, useFitting : Bool) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    
    // Establish the output thumbnail rectangle
    let targetRect = CGRect.from(size: targetSize)
    
    // Create the source image's bounding rectangle
    let naturalRect = CGRect.from(size: self.size)
    
    // Calculate fitting or filling destination rectangle
    let destinationRect = useFitting ? naturalRect.fit(into: targetRect) :
      naturalRect.fill(into: targetRect)
    
    // Draw the new thumbnail
    draw(in: destinationRect)
    
    // Retrieve and return the new image
    guard let thumbnail = UIGraphicsGetImageFromCurrentImageContext() else {
      return nil
    }
    
    return thumbnail
  }
  
  // This is only good for color images.
  func bytesFromRGB() -> Data? {
    guard let cgImage = self.cgImage else {
      return nil
    }
    // Establish color space
    
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    
    // Establish context
    let width = Int(size.width)
    let height = Int(size.height)
    
    guard let context = CGContext(data: nil, width: width, height: height,
      bitsPerComponent: 8, bytesPerRow: width * 4, space: colorSpace,
      bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue) else {
        
      return nil
    }
    
    // Draw source into context bytes
    let rect = CGRect.from(size: size)
    context.draw(cgImage, in: rect)
    
    // Create Data from bytes
    guard let cgData = context.data else {
      return nil
    }
    
    let data = Data(bytes: cgData, count: width * height * 4)
    
    return data
  }
  
  // Similarly, the expectation here is that the Data represents a color
  // image.
  static func fromBytesRGB(data: Data, targetSize: CGSize) -> UIImage? {
    // check data
    let width = Int(targetSize.width)
    let height = Int(targetSize.height)
    
    if data.count < (width * height * 4) {
      return nil
    }
    // Create a colorspace
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    
    var mutableData = data
    var image : UIImage?

    // How long did this take to figure out?   argh.  That was (not) easy.
    mutableData.withUnsafeMutableBytes(
      { (bytes : UnsafeMutablePointer<UInt8>)-> () in
        
      guard let context = CGContext(data: bytes, width: width, height: height,
        bitsPerComponent: 8, bytesPerRow: width * 4, space: colorSpace,
        bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue) else {
                                      
        return
      }
      
      // Convert to image
      guard let imageRef = context.makeImage() else {
        return
      }
      image = UIImage(cgImage: imageRef)
    })
    
    return image
  }
  
  /// Graphics often contain drawn embellishments like badges, or other things
  /// such that it's desirable for the image to have an alignment rect
  /// for layout use that is inset from the bounds of the image.   Use this
  /// method before calling UIImage.withAlignmentRectInsets in order to get
  /// AutoLayout to recognize that the intrinsic size of the image is within
  /// the bounds of the image.  If you know the alignment rect you want, 
  /// and the overall image bounds, this method can be used to build a set of
  /// UIEdgeInsets for use with the withAlignmentRectInsets call.
  ///
  /// - Parameters:
  ///   - withAlignmentRect: The inner rect you want Auto Layout to use.
  ///   - forImageBounds: The overall bounds of the image.
  /// - Returns: a set of UIEdgeInsets for use with 
  ///   UIImage.withAlignmentRectInsets
  static func buildInsets(forAlignmentRect alignmentRect: CGRect,
    withImageBounds imageBounds : CGRect) -> UIEdgeInsets {
    
    // Ensure alignment rect is fully within source
    let targetRect = imageBounds.intersection(alignmentRect)
    
    // Calculate insets
    var insets = UIEdgeInsets()
    insets.left = targetRect.minX - imageBounds.minX
    insets.right = imageBounds.maxX - targetRect.maxX
    insets.top = targetRect.minY - imageBounds.minY
    insets.bottom = imageBounds.maxY - targetRect.maxY
    
    return insets
  }
}
