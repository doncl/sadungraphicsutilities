//
//  UIBezierPathExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore

//MARK: Bounds
public extension UIBezierPath {
  var boundingBox : CGRect {
    return cgPath.boundingBoxOfPath
  }
  
  /// Gets bounding box, accounting for the stroke, one half of stroke
  /// lies inside, one half outside.
  var boundingBoxWithLineWidth : CGRect {
    let bounds = boundingBox
    return bounds.insetBy(dx: -lineWidth / 2.0, dy: -lineWidth / 2.0)
  }
  
  var boundingBoxCenter : CGPoint {
    return boundingBox.center
  }
  
  //MARK: Misc
  func clip(to rect: CGRect) {
    UIBezierPath(rect: rect).addClip()
  }
  
  static func fill(rect : CGRect, withColor color: UIColor) {
    UIBezierPath(rect: rect).fill(withColor: color)
  }
}
  //MARK: Transforms
  
public extension UIBezierPath {
  // It's kind of counter-intuitive why you need to translate
  // the path to the center of the bounding box; there's a good video diagram
  // of it in Wenderlich's Core Video courses (either Beginner or Intermediate,
  // can't remember).   But basically, you need to move the  entire path over
  // such that its edge is exactly on the center of where it was, then apply
  // the desired transform, then move it back.
  func applyCentered(transform: CGAffineTransform) {
    let center = boundingBoxCenter
    var t : CGAffineTransform = .identity
    t = t.translatedBy(x: center.x, y: center.y)
    t = transform.concatenating(t)
    t = t.translatedBy(x: -center.x, y: -center.y)
    apply(t)
  }
  
  func path(byApplyingTransform transform : CGAffineTransform)
    -> UIBezierPath {
      
      let pathCopy = self.copy() as! UIBezierPath
      pathCopy.applyCentered(transform: transform)
      return pathCopy
  }
  
  // In radians, of couse.
  func rotate(angleInRadians : CGFloat) {
    let t = CGAffineTransform(rotationAngle: angleInRadians)
    applyCentered(transform: t)
  }
  
  func scale(sx : CGFloat, sy: CGFloat) {
    let t = CGAffineTransform(scaleX: sx, y: sy)
    applyCentered(transform: t)
  }
  
  func offset(by offset: CGSize) {
    let t = CGAffineTransform(translationX: offset.width, y: offset.height)
    applyCentered(transform: t)
  }
  
  func movePath(to destPoint: CGPoint) {
    let bounds = boundingBox
    let p1 = bounds.origin
    let p2 = destPoint
    let vector = CGSize(width: p2.x - p1.x, height: p2.y - p1.y)
    offset(by: vector)
  }
  
  func moveCenter(to destPoint : CGPoint) {
    let bounds = boundingBox
    let p1 = bounds.origin
    let p2 = destPoint
    var vector = CGSize(width: p2.x - p1.x, height: p2.y - p1.y)
    vector.width -= bounds.size.width / 2.0
    vector.height -= bounds.size.height / 2.0
    offset(by: vector)
  }
  
  func mirrorHorizontally() {
    let t = CGAffineTransform(scaleX: -1, y: 1)
    applyCentered(transform: t)
  }
  
  func mirrorVertically() {
    let t = CGAffineTransform(scaleX: 1, y: -1)
    applyCentered(transform: t)
  }
}

//MARK: Fitting to rect
public extension UIBezierPath {
  func fit(to destRect: CGRect) {
    let bounds = boundingBox
    let fitRect = bounds.fit(into: destRect)
    let scaleValue = bounds.aspectScaleFit(destSize: destRect.size)
    let newCenter = fitRect.center
    moveCenter(to: newCenter)
    scale(sx: scaleValue, sy: scaleValue)
  }
  
  func adjust(to destRect: CGRect) {
    let bounds = boundingBox
    if bounds.width == 0 || bounds.height == 0 {
      return // nothing we can do.
    }
    let scaleX = destRect.size.width / bounds.size.width
    let scaleY = destRect.size.height / bounds.size.height
    
    let newCenter = destRect.center
    moveCenter(to: newCenter)
    scale(sx: scaleX, sy: scaleY)
  }
}
  
  
//MARK: Path Attributes
public extension UIBezierPath {
  func addDashes() {
    let dashes : [CGFloat] = [6, 2]
    setLineDash(dashes, count: 2, phase: 0)
  }
  
  func copyDashes(to destination : UIBezierPath) {
    var count : Int = 0
    getLineDash(nil, count: &count, phase: nil)
    var phase : CGFloat = 0
    var pattern : [CGFloat] = Array(repeating: 0.0, count: count)
    getLineDash(&pattern, count: &count, phase: &phase)
    destination.setLineDash(pattern, count: count, phase: phase)
  }
  
  func copyState(to destPath : UIBezierPath) {
    destPath.lineWidth = lineWidth
    destPath.lineCapStyle = lineCapStyle
    destPath.lineJoinStyle = lineJoinStyle
    destPath.miterLimit = miterLimit
    destPath.flatness = flatness
    destPath.usesEvenOddFillRule = usesEvenOddFillRule
    copyDashes(to: destPath)
  }
}


//MARK: Text
public extension UIBezierPath {
  static func from(string: String, withFont font : UIFont) -> UIBezierPath? {
    if string.count == 0 {
      return nil
    }
    // init path
    let path = UIBezierPath()

    
    // Create font ref
    let fontRef = CTFontCreateWithName(font.fontName as CFString, font.pointSize,
      nil)
    
    // Create glyphs
    var glyphs : [CGGlyph] = Array(repeating: 0, count: string.count)
    
    let nsString : NSString = NSString(string:string)

    guard let cstr = nsString.utf8String else {
      return nil
    }
    var unichars : [UniChar] = Array(repeating: 0, count: nsString.length)
    for i in 0..<nsString.length {
      unichars[i] = UniChar(cstr[i])
    }
    
    if false == CTFontGetGlyphsForCharacters(fontRef, unichars, &glyphs,
      nsString.length) {
      
      return nil
    }
    
    // Draw each glyph into path
    for i in 0..<nsString.length {
      let glyph = glyphs[i]
      if let pathRef = CTFontCreatePathForGlyph(fontRef, glyph, nil) {
        let b = UIBezierPath(cgPath: pathRef)
        path.append(b)
      }
      let subString = nsString.substring(with: NSMakeRange(i, 1))
      let size = subString.size(withAttributes: [NSAttributedString.Key.font : font])
      path.offset(by: CGSize(width: -size.width, height: 0))
    }
    
    // Math
    path.mirrorVertically()
    return path
  }
  
  static func from(string: String, withFontFace : String) -> UIBezierPath? {
    var fontOpt = UIFont(name: withFontFace, size: 16.0)
    if fontOpt == nil {
       fontOpt = UIFont.systemFont(ofSize: 16.0)
    }
    guard let font = fontOpt else {
      return nil
    }
    return UIBezierPath.from(string: string, withFont: font)
  }
  
  static func textImage(from string: String, withFontFace fontFace : String,
    pointSize: CGFloat = 24.0, renderSize : CGSize) -> UIImage? {
    
    var fontOpt = UIFont(name: fontFace, size: pointSize)
    if fontOpt == nil {
      fontOpt = UIFont.systemFont(ofSize: 16.0)
    }
    guard let font = fontOpt else {
      return nil
    }
    
    return textImage(from: string, withFont: font, renderSize: renderSize)
  }
  
  static func textImage(from string: String, withFont font : UIFont,
    renderSize: CGSize) -> UIImage? {
    
    return pushGetImage(with: {(r : CGRect) in
      guard let p = UIBezierPath.from(string: string, withFont: font) else {
        return
      }
      p.fit(to: r)
      p.fill()
      p.stroke()
      
    }, andSize: renderSize)
  }
}
  //MARK: Polygon fun
public extension UIBezierPath {
  static func polygon(numberOfSides : Int) -> UIBezierPath? {
    guard numberOfSides > 2 else {
      return nil
    }
    
    let destinationRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    
    let path = UIBezierPath()
    let center = destinationRect.center
    let r : CGFloat = 0.5  // radius
    
    var firstPoint : Bool = true
    
    for i in 0..<numberOfSides {
      let theta = CGFloat.pi +
        CGFloat(i) * 2.0 * CGFloat.pi / CGFloat(numberOfSides)
      
      let dTheta : CGFloat = CGFloat.pi * 2.0 / CGFloat(numberOfSides)
      
      if firstPoint {
        let x = center.x + r * sin(theta)
        let y = center.y + r * cos(theta)
        let p : CGPoint = CGPoint(x: x, y: y)
        path.move(to: p)
        firstPoint = false
      }
      
      let x = center.x + r * sin(theta + dTheta)
      let y = center.y + r * cos(theta + dTheta)
      let p : CGPoint = CGPoint(x: x, y: y)
      path.addLine(to: p)
    }
    
    path.close()
    
    return path
  }
  
  static func inflectedShape(numberOfInflections: Int,
    percentInflection : CGFloat) -> UIBezierPath? {
    
    guard numberOfInflections > 2 else {
      return nil
    }
    
    let path = UIBezierPath()
    let destinationRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    let center = destinationRect.center
    let r : CGFloat = 0.5
    let rr = r * (1.0 + percentInflection)
    
    var firstPoint = true
    
    // Cast the int to CGFloat just once.
    let fNumberOfInflections : CGFloat = CGFloat(numberOfInflections)
    
    for i in 0..<numberOfInflections {
      let theta = CGFloat(i) * CGFloat.pi * 2.0 / fNumberOfInflections
      let dTheta = CGFloat.pi * 2.0 / fNumberOfInflections
      
      if firstPoint {
        let xa : CGFloat  = center.x + r * sin(theta)
        let ya : CGFloat = center.y + r * cos(theta)
        let pa : CGPoint = CGPoint(x: xa, y: ya)
        path.move(to: pa)
        firstPoint = false
      }
      
      let cp1x : CGFloat = center.x + rr * sin(theta + dTheta / 3)
      let cp1y : CGFloat = center.y + rr * cos(theta + dTheta / 3)
      let cp1 : CGPoint = CGPoint(x: cp1x, y: cp1y)
      
      let cp2x : CGFloat = center.x + rr * sin(theta + 2 * dTheta / 3)
      let cp2y : CGFloat = center.y + rr * cos(theta + 2 * dTheta / 2)
      let cp2 : CGPoint = CGPoint(x: cp2x, y: cp2y)
      
      let xb : CGFloat = center.x + r * sin(theta + dTheta)
      let yb : CGFloat = center.y + r * cos(theta + dTheta)
      let pb : CGPoint = CGPoint(x: xb, y: yb)
      
      path.addCurve(to: pb, controlPoint1: cp1, controlPoint2: cp2)
    }
    
    path.close()
    
    return path
  }
  
  static func starShape(numberOfInflections: Int, percentInflection : CGFloat)
    -> UIBezierPath? {
    
    guard numberOfInflections > 2 else {
      return nil
    }
      
    let path = UIBezierPath()
    let destinationRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    let center = destinationRect.center
    let r : CGFloat = 0.5
    let rr = r * (1.0 + percentInflection)
      
    var firstPoint = true
     
    // Cast the int to CGFloat just once.
    let fNumberOfInflections : CGFloat = CGFloat(numberOfInflections)
  
    for i in 0..<numberOfInflections {
      let theta = CGFloat(i) * CGFloat.pi * 2.0 / fNumberOfInflections
      let dTheta = CGFloat.pi * 2.0 / fNumberOfInflections
      
      if firstPoint {
        let xa = center.x + r * sin(theta)
        let ya = center.y + r * cos(theta)
        let pa = CGPoint(x: xa, y: ya)
        path.move(to: pa)
        firstPoint = false
      }
      
      let cp1x = center.x + rr * sin(theta + dTheta / 2)
      let cp1y = center.y + rr * cos(theta + dTheta / 2)
      let cp1 = CGPoint(x: cp1x, y: cp1y)
      
      let xb = center.x + r * sin(theta + dTheta)
      let yb = center.y + r * cos(theta + dTheta)
      let pb = CGPoint(x: xb, y: yb)
      
      path.addLine(to: cp1)
      path.addLine(to: pb)
    }
    
    path.close()
   
    return path
  }
}

//MARK: Shadows
public extension UIBezierPath {
  func setShadow(color : UIColor?, size: CGSize, blur: CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.setShadow(offset: size, blur: blur, color: color?.cgColor)
  }
  
  // Draw *only* the shadow
  func drawShadow(color: UIColor, size: CGSize, blur: CGFloat) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    // Build shadow
    pushDraw(block: {
      self.setShadow(color: color, size: size, blur: blur)
      self.inverse.addClip()
      self.fill(withColor: color)
    })
  }
}

//MARK: Handy Utilities
public extension UIBezierPath {
  func addDashes(withPattern pattern: [CGFloat]) {
    guard pattern.count > 0 else {
      return
    }
    
    let patternCopy = Array(pattern)
    setLineDash(patternCopy, count: patternCopy.count, phase: 0.0)
  }
  
  func applyPropertiesToContext() {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.setLineWidth(lineWidth)
    context.setLineCap(lineCapStyle)
    context.setLineJoin(lineJoinStyle)
    context.setMiterLimit(miterLimit)
    context.setFlatness(flatness)
    
    var count : Int = 0
    getLineDash(nil, count: &count, phase: nil)
    if count == 0 {
      return
    }
    
    var phase : CGFloat = 0.0
    var pattern : [CGFloat] = Array(repeating: 0.0, count: count)
    getLineDash(&pattern, count: &count, phase: &phase)
    context.setLineDash(phase: phase, lengths: pattern)
  }
  
  func stroke(withWidth width: CGFloat, andColor color: UIColor? = nil) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      if let color = color {
        color.setStroke()
      }
      let holdWidth = self.lineWidth
      if width > 0.0 {
        self.lineWidth = width
      }
      self.stroke()
      self.lineWidth = holdWidth
    })
  }

  func strokeInside(withWidth width : CGFloat?, andColor color : UIColor? = nil) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      if let color = color {
        color.setStroke()
      }
      let holdWidth = self.lineWidth
      if let width = width, width > 0.0 {
        self.lineWidth = width * 2.0
      } else {
        self.lineWidth = self.lineWidth * 2.0
      }
      self.addClip()
      self.stroke()
      self.lineWidth = holdWidth
    })
  }
  
  func fill(withColor color: UIColor?) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      if let color = color {
        color.setFill()
      }
      self.fill()
    })
  }
  
  func fill(withColor color: UIColor?, withMode mode: CGBlendMode) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      context.setBlendMode(mode)
      self.fill(withColor: color)
    })
  }
}

//MARK: - Clippage
public extension UIBezierPath {
  
  // Not sure why Sadun has this helper, since....it doesn't do anything.
  // but it's used later on, in functions like innerGlow, so making it look 
  // the same makes it less errorprone for me to pilfer her code.
  func clipToPath() {
    addClip()
  }
  
  func clip(toStrokeWidth width: Int) {
    let pathRef = cgPath.copy(strokingWithWidth: CGFloat(width), lineCap: .butt, lineJoin: .miter, miterLimit: 0.4)
    let clipPath = UIBezierPath(cgPath: pathRef)
    clipPath.addClip()
  }
}

//MARK: - Misc
public extension UIBezierPath {
  func safeCopy() -> UIBezierPath {
    let p = UIBezierPath()
    p.append(self)
    copyState(to: p)
    return p
  }
}

//MARK: - Elements

// Retrieve array of component elements
public extension UIBezierPath {
  var elements : [BezierElement] {
    var eles : [BezierElement] = []
    
    withUnsafeMutablePointer(to: &eles, { (arrayPointer : UnsafeMutablePointer) in
      cgPath.apply(info: arrayPointer) { elems, elementPointer in
        let bezierElement = BezierElement.element(with: elementPointer.pointee)
        
        guard let elems = elems else {
          fatalError("don't be daft.")
        }
        let a = elems.assumingMemoryBound(to: [BezierElement].self)
        a.pointee.append(bezierElement)
      }
    })
       return eles
  }
  
  static func path(from elements : [BezierElement]) -> UIBezierPath {
    let path = UIBezierPath()
    for element in elements {
      element.add(to: path)
    }
    return path
  }
}

//MARK:  Path length
public extension UIBezierPath {
  var pathLength : CGFloat {
    let eles = self.elements
    var current : CGPoint = CGPoint.Nil
    var firstPoint : CGPoint = CGPoint.Nil
    var totalPathLength : CGFloat = 0.0
    
    // walk through path element in the BezierPath.
    for ele in eles {
      // It's okay for current and firstpoint to be nil the first time through.
      // since, any legal bezier path has a moveToPoint as first element.
      totalPathLength += ele.distance(from: current, withStartPoint: firstPoint)
      if ele.type == .moveToPoint {
        firstPoint = ele.point
      } else if ele.type == .closeSubpath {
        firstPoint = CGPoint.Nil
      }
 
      if ele.type != .closeSubpath {
        current = ele.point
      }
    }
    return totalPathLength
  }
}

//MARK: Path Interpolation
public extension UIBezierPath {
  func pointAndSlope(atPercent percent : CGFloat, calculateSlope : Bool = false)
    -> (CGPoint, CGPoint?) {
    
    // Retrieve path elements
    let eles = self.elements
    if eles.count == 0 {
      return (CGPoint.Nil, nil)
    }
    
    if percent == 0.0 {
      let first = eles[0]
      return (first.point, nil)
    }
    
    // Retrieve the full path distance - cache it so it's not being recalc'ed.
    let cachedPathLength = self.pathLength
    var totalDistance : CGFloat = 0.0
    
    // Establish the current and firstPoint states
    var current : CGPoint = CGPoint.Nil
    var firstPoint : CGPoint = CGPoint.Nil
    
    // Iterate through elements until the percentage no longer overshoots
    for ele in eles {
      let distance = ele.distance(from: current, withStartPoint: firstPoint)
      let proposedTotalDistance = totalDistance + distance
      
      let proposedPercent = proposedTotalDistance / cachedPathLength
      
      if proposedPercent < percent {
        // Consume and continue
        totalDistance = proposedTotalDistance
        
        if ele.type == .moveToPoint {
          firstPoint = ele.point
        }
        
        current = ele.point
        
        continue
      }
      
      // What percent between p1 and p2?
      let currentPercent = totalDistance / cachedPathLength
      let dPercent = percent - currentPercent
      let percentDistance = dPercent * cachedPathLength
      let targetPercent = percentDistance / distance
      
      // return result
      let pointAndOptionalSlope = interpolatePoint(from: ele, point: current,
        startPoint: firstPoint, percent: targetPercent,
        calculateSlope: calculateSlope)
   
      return pointAndOptionalSlope
    }
    
    
    return (CGPoint.Nil, nil)
  }
  
  fileprivate func interpolatePoint(from element: BezierElement, point: CGPoint,
    startPoint: CGPoint, percent : CGFloat, calculateSlope : Bool ) -> (CGPoint, CGPoint?) {
    
    var slope : CGPoint? = nil
    switch element.type {
    case .moveToPoint:
      // No distance
      if calculateSlope {
        slope = CGPoint(x: CGFloat.infinity, y: CGFloat.infinity)
      }
      return (point, slope)
      
    case .closeSubpath:
      // from self.point to firstPoint
      let tuple = point.interpolateLineSegment(to: startPoint,
        percent: percent)
      if calculateSlope {
        slope = tuple.1
      }
      return (tuple.0, slope)
      
    case .addLineToPoint:
      // from point to self.point
      let tuple = point.interpolateLineSegment(to: element.point,
        percent: percent)
      if calculateSlope {
        slope = tuple.1
      }
      return (tuple.0, slope)
      
    case .addQuadCurveToPoint:
      // From point to self.point
      let p = point.quadBezierPoint(progress: percent,
        controlPoint1: element.controlPoint1, end: element.point)
      
      if calculateSlope {
        // Calculate slope by moving back slightly
        let backPercent = percent * 0.9
        let dx = p.x - CGPoint.quadBezier(progress: backPercent,
          startValue: point.x, controlValue1: element.controlPoint1.x,
          endValue: element.point.x)
        
        let dy = p.y - CGPoint.quadBezier(progress: backPercent,
          startValue: point.y, controlValue1: element.controlPoint1.y,
          endValue: element.point.y)
        
        slope = CGPoint(x: dx, y: dy)
      }
      return (p, slope)
      
    case .addCurveToPoint:
      // From point to self.point
      let p = point.cubicBezierPoint(progress: percent,
        controlPoint1: element.controlPoint1,
        controlPoint2: element.controlPoint2, end: element.point)
 
      if calculateSlope {
        // Calculate slope by moving back slightly
        let backPercent = percent * 0.9
        let dx = p.x - CGPoint.cubicBezier(progress: backPercent,
          startValue: point.x, controlValue1: element.controlPoint1.x,
          controlValue2: element.controlPoint2.x, endValue: element.point.x)
        
        let dy = p.x - CGPoint.cubicBezier(progress: backPercent,
          startValue: point.y, controlValue1: element.controlPoint1.y,
          controlValue2: element.controlPoint2.y, endValue: element.point.y)
        
        slope = CGPoint(x: dx, y: dy)
      }
      return (p, slope)
      
    @unknown default:
      print("Unknown case \(element.type)")
      return (point, nil)
    }
  }
}

//MARK: Subpaths 
public extension UIBezierPath {
  var subPaths : [UIBezierPath] {
    var results : [UIBezierPath] = []
    var current : UIBezierPath? = nil
    
    let eles = self.elements
    
    for element in eles {
      // Close the subpath and add to the results
      if element.type == .closeSubpath {
        if let currentUnwrapped = current {
          currentUnwrapped.close()
          results.append(currentUnwrapped)
          current = nil
          continue
        }
      }
      
      // Begin new subpaths on moveToPoint
      if element.type == .moveToPoint {
        if let currentUnwrapped = current {
          results.append(currentUnwrapped)
        }
        current = UIBezierPath()
        current?.move(to: element.point)
        continue
      }
      
      if let current = current {
        element.add(to: current)
      } else {
        // cannot add to nil path
        continue
      }
    }
    if let currentUnwrapped = current {
      results.append(currentUnwrapped)
    }
    return results
  }
}

//MARK: Inverting path
public extension UIBezierPath {
  func inverse(into rect : CGRect) -> UIBezierPath {
    let path = UIBezierPath()
    path.append(self)
    let rectPath = UIBezierPath(rect: rect)
    path.append(rectPath)
    path.usesEvenOddFillRule = true
    return path
  }
  
  var inverse : UIBezierPath {
    return inverse(into: CGRect.infinite)
  }
  
  var boundedInverse : UIBezierPath {
    return inverse(into: bounds)
  }
}

//MARK: Drawing an inner shadow
public extension UIBezierPath {
  func drawInnerShadow(color : UIColor?, size : CGSize, blur : CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    // Build shadow
    context.saveGState()
    context.setShadow(offset: size, blur: blur, color: color?.cgColor)
    
    // clip to the original path
    addClip()
    
    // Fill the inverted path
    inverse.fill(withColor: color)
    context.restoreGState()
  }
  
  func drawBetterInnerShadow(color: UIColor, size: CGSize,
    blur blurRadius: CGFloat) {
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.saveGState()
    
    // Original inspired by the PaintCode guys, and then ripped off from
    // Erica Sadun's Obj-C code;.
    
    // Establish initial offsets
    var xOffset = size.width
    let yOffset = size.height
    
    // Adjust the border
    let pathBounds = self.bounds
    var borderRect = pathBounds.insetBy(dx: -blurRadius, dy: -blurRadius)
    borderRect = borderRect.offsetBy(dx: -xOffset, dy: -yOffset)
    let unionRect = borderRect.union(pathBounds)
    borderRect = unionRect.insetBy(dx: -1, dy: -1)
    
    // Tweak the size a tiny bit
    xOffset += round(borderRect.size.width)
    let tweakSize = CGSize(width: xOffset + copysign(0.1, xOffset),
                           height: yOffset + copysign(0.1, yOffset))
    
    // Set the shadow and clip
    context.setShadow(offset: tweakSize, blur: blurRadius, color: color.cgColor)
    
    addClip()
    
    // Apply transform
    let transform =
      CGAffineTransform(translationX: -round(borderRect.size.width), y: 0)
    
    let negativePath = inverse(into: borderRect)
    negativePath.apply(transform)
    
    negativePath.fill(withColor: color)
    
    context.restoreGState()
  }
}

//MARK: Glows
public extension UIBezierPath {
  func drawOuterGlow(fillColor : UIColor, withRadius radius : CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.saveGState()
    
    inverse.clipToPath()
    
    context.setShadow(offset: .zero, blur: radius, color: fillColor.cgColor)
    
    fill(withColor: UIColor.gray)
  
    context.restoreGState()
  }
  
  func drawInnerGlow(fillColor : UIColor, withRadius radius : CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    context.saveGState()
    clipToPath()
    
    context.setShadow(offset: .zero, blur: radius, color: fillColor.cgColor)
    
    inverse.fill(withColor: UIColor.gray)
    
    context.restoreGState()
  }
}

//MARK: Reversing Bezier paths
public extension UIBezierPath {
  private func reverseSubPath() -> UIBezierPath {
    let eles = self.elements
  
    let reversedElements : [BezierElement] = eles.reversed()
    
    let newPath = UIBezierPath()
    var closesSubpath = false
    
    // Locate the element with the first point
    var firstElement : BezierElement? = nil
    for ele in eles {
      if false == ele.point.isNil {
        firstElement = ele
        break
      }
    }
    
    // locate the element with the last point
    var lastElement : BezierElement? = nil
    for ele in reversedElements {
      if false == ele.point.isNil {
        lastElement = ele
      }
    }
    
    // Check for path closure
    if let ele = eles.last {
      if ele.type == .closeSubpath {
        if let firstElement = firstElement {
          newPath.move(to: firstElement.point)
        }
        
        if let lastElement = lastElement {
          newPath.addLine(to: lastElement.point)
        }
        
        closesSubpath = true
      } else {
        if let lastElement = lastElement {
          newPath.move(to: lastElement.point)
        }
      }
      
      // Iterate backwards and reconstruct the path
      var i : Int = 0
      
      for element in reversedElements {
        i += 1
        var nextElement : BezierElement? = nil
        let workElement : BezierElement = element.copy() as! BezierElement
        
        if element.type == .closeSubpath {
          continue
        }
        
        if let firstElement = firstElement, element == firstElement {
          if closesSubpath {
            newPath.close()
            continue
          }
        }
        
        if i < reversedElements.count {
          nextElement = reversedElements[i]
          if false == workElement.controlPoint2.isNil {
            // swap control points
            let tmp = workElement.controlPoint1
            workElement.controlPoint1 = workElement.controlPoint2
            workElement.controlPoint2 = tmp
          }
          workElement.point = (nextElement?.point)!
        }
        
        if element.type == .moveToPoint {
          workElement.type = .addLineToPoint
        }
        
        workElement.add(to: newPath)
      }
    }
    return newPath
  }
  
  // Reverse the entire path
  var reversed : UIBezierPath {
    let reversedPath = UIBezierPath()
    let reversedSubPaths : [UIBezierPath] = self.subPaths.reversed()
    
    for subpath in reversedSubPaths {
      let reversedSubPath = subpath.reverseSubPath()
      reversedPath.append(reversedSubPath)
    }
    return reversedPath
  }
}


// Show path progression
public extension UIBezierPath {
  func showPathProgression(maxPercent : CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    let distance = pathLength
    guard distance > 0 else {
      return
    }
    
    // Bound the percent value
    let maximumPercent : CGFloat = max(min(maxPercent, 1.0), 0.0)
    
    context.saveGState()
    defer {
      context.restoreGState()
    }
    
    // One sample every six points
    let samples = distance / 6.0
    
    guard samples > 0 else {
      return
    }
    
    // Change in white level for each sample
    let dLevel = 0.75 / samples
    
    let endLoop = Int(maximumPercent * samples)
    
    for i in 0..<endLoop {
      // Calculate progress and color
      let percent = CGFloat(i) / samples
      let pointAndNilSlope = pointAndSlope(atPercent: percent)
      let point = pointAndNilSlope.0
      let color = UIColor(white: CGFloat(i) * dLevel, alpha: 1.0)
      
      // Draw marker
      let r = CGRect.rectAroundCenter(center: point,
        size: CGSize(width: 2, height: 2))
      
      let marker = UIBezierPath(ovalIn: r)
      marker.fill(withColor: color)
    }
  }
}

//MARK:  Text-y, shadow-y stuff
public extension UIBezierPath {
  static func drawStrokedShadowedText(string: String, fontFace : String,
                               baseColor : UIColor, dest: CGRect) {
    
    // Create text path
    guard let t = UIBezierPath.from(string: string, withFontFace: fontFace) else {
      return
    }
    t.fit(to: dest)
    t.drawStrokedShadowedShape(baseColor: baseColor, dest: dest)
  }
  
  func drawStrokedShadowedShape(baseColor : UIColor, dest : CGRect) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      let offset : CGSize = CGSize(width: 4, height: 4)
      
      context.setShadow(offset: offset, blur: 4)
      
      pushLayerDraw(block: {
        // Push letter gradient (to half brightness)
        pushDraw(block: {
          let scaledColor = baseColor.scaleBrightness(amount: 0.5)
          guard let innerGradient = Gradient.from(color1: baseColor,
            to: scaledColor) else {
            
            return
          }
          self.addClip()
          innerGradient.drawTopToBottom(rect: self.bounds)
        })
        
        // Add the inner shadow with darker color
        pushDraw(block: {
          context.setBlendMode(.multiply)
          let innerColor : UIColor = baseColor.scaleBrightness(amount: 0.3)
          let innerOffset : CGSize = CGSize(width: 0, height: -2)
          self.drawInnerShadow(color: innerColor, size: innerOffset, blur: 2)
        })
        
        // Stroke with reversed gray gradient
        pushDraw(block: {
          self.clip(toStrokeWidth: 6)
          self.inverse.addClip()
          let c1: UIColor = UIColor(white: 0.0, alpha: 1.0)
          let c2 : UIColor = UIColor(white: 0.5, alpha: 1.0)
          guard let grayGradient = Gradient.from(color1: c1, to: c2) else {
            return
          }
          grayGradient.drawTopToBottom(rect: dest)
        })
      })
    })
  }
  
  func drawIndented(primary : UIColor, rect: CGRect) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      guard let context = UIGraphicsGetCurrentContext() else {
        return
      }
      context.setBlendMode(.multiply)
      let color = UIColor(white: 0.0, alpha: 0.4)
      let size = CGSize(width: 0, height: 2)
      
      self.drawInnerShadow(color: color, size: size, blur: 1)
    })
    
    let shadowColor = UIColor(white: 1.0, alpha: 0.5)
    let offset = CGSize(width: 0, height: 2)
    let bevelColor = UIColor(white: 0.0, alpha: 0.4)
    
    drawShadow(color: shadowColor, size: offset, blur: 1.0)
    bevel(color: bevelColor, radius: 2.0, theta: 0)
    
    pushDraw(block: {
      guard let context = UIGraphicsGetCurrentContext() else {
        return
      }
      self.addClip()
      context.setAlpha(0.3)
      
      let secondary = primary.scaleBrightness(amount: 0.3)
      guard let gradient = Gradient.from(color1: primary, to: secondary) else {
        return
      }
      gradient.drawBottomToTop(rect: self.bounds)
    })
  }
  
  // Create 3d embossed effect - typically call with black color at 0.5
  func emboss(color: UIColor, radius: CGFloat, blur : CGFloat) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    let contrast = color.contrast
    let size1 : CGSize = CGSize(width: -radius, height: radius)
    let size2 : CGSize = CGSize(width: radius, height: -radius)
    drawInnerShadow(color: contrast, size: size1, blur: blur)
    drawInnerShadow(color: color, size: size2, blur: blur)
  }
  
  // Typically call with black color at 0.5
  func bevel(color: UIColor, radius: CGFloat, theta : CGFloat) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      // cain't draw into nuthin'
      return
    }
    
    let x : CGFloat = radius * sin(theta)
    let y : CGFloat = radius * cos(theta)
    
    let innerShadowOffset : CGSize = CGSize(width: -x, height: y)
    drawInnerShadow(color: color, size: innerShadowOffset, blur: 2)
    let shadowOffset : CGSize = CGSize(width: x / 2.0, height: -y / 2.0)
    drawShadow(color: color, size: shadowOffset, blur: 0)
  }
  
  static func drawIndentedText(string: String, fontFace : String, primary: UIColor,
    rect : CGRect) {
    
    guard let letterPath = UIBezierPath.from(string: string,
      withFontFace: fontFace) else {
                                              
      return
    }
    
    letterPath.fit(to: rect)
    letterPath.drawIndented(primary: primary, rect: rect)
  }
}

//MARK: Drawing over Textures
public extension UIBezierPath {
  
  func draw(overTexture texture: UIImage, usingGradient gradient: Gradient,
    withAlpha alpha: CGFloat) {
    
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }

    let rect = bounds
    
    pushDraw(block: {
      context.setAlpha(alpha)
      self.addClip()
      pushLayerDraw(block: {
        texture.draw(in: rect)
        context.setBlendMode(.color)
        gradient.drawTopToBottom(rect: rect)
      })
    })
  }
  
  func fillWithNoise(fillColor : UIColor) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    assert(Thread.isMainThread, "Do Not call off main thread")
    let noiseColor = UIColor.noise
    fill(withColor: fillColor)
    let colorToUse = noiseColor.withAlphaComponent(0.05)
    fill(withColor: colorToUse, withMode: .screen)
  }
  
  func drawBottomGlow(color: UIColor, percent: CGFloat) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    let rect = calculatedBounds
    let h1 = rect.pointAtPercents(xPercent: 0.5, yPercent: 1.0)
    let h2 = rect.pointAtPercents(xPercent: 0.5, yPercent: 1.0 - percent)
    
    let c2 : UIColor = color.withAlphaComponent(0.0)
    guard let gradient = Gradient.easeInOut(between: color, and: c2) else {
      return
    }
    
    pushDraw(block: {
      self.addClip()
      gradient.draw(from: h1, to: h2)
    })
  }
  
  func drawIconTopLight(p : CGFloat) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    let percent : CGFloat = 1.0 - p
    let rect = bounds
    var offset : CGRect = rect
    offset.origin.y -= percent * offset.size.height
    offset = offset.insetBy(dx: -offset.size.width * 0.3, dy: 0.0)
    
    let ovalPath = UIBezierPath(ovalIn: offset)
    let c1 : UIColor = UIColor(white: 1, alpha: 0)
    let c2 : UIColor = UIColor(white: 1, alpha: 0.5)
    
    guard let gradient = Gradient.from(color1: c1, to: c2) else {
      return
    }
    
    pushDraw(block: {
      self.addClip()
      ovalPath.addClip()
      
      // Draw gradient
      let p1 = rect.pointAtPercents(xPercent: 0.5, yPercent: 0.0)
      let ovalRect = ovalPath.bounds
      let p2 = ovalRect.pointAtPercents(xPercent: 0.5, yPercent: 1)
      gradient.draw(from: p1, to: p2)
    })
  }
  
  var calculatedBounds : CGRect {
    return cgPath.boundingBoxOfPath
  }
}














