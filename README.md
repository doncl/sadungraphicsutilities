# SadunGraphicsUtilities

Slavish Swift translation of the awesome Erica Sadun's UIKit Drawing Utilities from Addison Wesley Books.   All credit goes to her, any mistakes in translation are mine. 

I had more fun spending a week reading, studying, and translating this stuff to Swift during a week's vacay to Maui in summer of 2017 (I believe); I mean, way more fun
than anyone should be allowed to have.

My wife and daughter were off snorkeling, but I was in a nice hotel room (we splurged for that trip), happily studying away, and learning a ton.
I know it says something disturbing about my personality that I would do this, but you know...guilty as charged.

I use these utilities to this day.  I have heard Erica is planning to rewrite the book in Swift, and I'll probably retire this code in favor of the real stuff 
when she does that.

